package webant.ru.application.core

import android.content.Context
import android.content.SharedPreferences

object Shared {
    private lateinit var sharedPreferences: SharedPreferences

    fun init(context: Context) {
        sharedPreferences = context.getSharedPreferences("shared", Context.MODE_PRIVATE)
    }

    private fun save(key: String, value: String) =
        sharedPreferences.edit().putString(key, value).apply()

    private fun readString(key: String, defaultValue: String) =
        sharedPreferences.getString(key, defaultValue)

    private fun save(key: String, value: Int) =
        sharedPreferences.edit().putInt(key, value).apply()

    private fun readInt(key: String, defaultValue: Int) =
        sharedPreferences.getInt(key, defaultValue)


}