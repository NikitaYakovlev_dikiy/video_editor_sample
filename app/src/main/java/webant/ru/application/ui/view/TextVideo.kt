package webant.ru.application.ui.view

import android.content.Context
import android.os.Environment
import org.m4m.MediaComposer
import ru.webant.video.editor.BaseVideoEditor
import ru.webant.video.view.BaseDrawerModel
import webant.ru.application.video.DrawerModel
import webant.ru.application.video.TextOverlay
import java.io.File

class TextVideo(context: Context) : BaseVideoEditor(context) {
    override var videoDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).path!!
    override var firstVideo = "$videoDirectory/fileName.mp4"
    override var outVideo = "$videoDirectory/outVideo1.mp4"
    lateinit var drawerModel: DrawerModel


    fun loadFileList() = File(videoDirectory).listFiles()
        .filter { it.name.split(".").last() == "mp4" }
        .map { it.name }

    fun createFile(
        firstFileName: String,
        outFileName: String,
        drawer: DrawerModel,
        onProgress: (Float) -> Unit,
        onComplete: (String) -> Unit
    ) {
        drawerModel = drawer
        firstVideo = "$videoDirectory/$firstFileName"
        outVideo = "$videoDirectory/$outFileName.mp4"
        this.onComplete = onComplete
        this.onProgress = onProgress
        startTranscode()
    }

    override fun setTranscodeParameters(mediaComposer: MediaComposer) {
        super.setTranscodeParameters(mediaComposer)
        mediaComposer.addVideoEffect(TextOverlay(0, factory!!.eglUtil, drawerModel))
    }

    override fun onError(message: Throwable) {
        message.printStackTrace()
    }
}
