package webant.ru.application.ui.scene.camera

import android.os.Bundle
import android.os.Environment
import android.view.SurfaceHolder
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_camera.*
import webant.ru.application.R
import webant.ru.application.loadImage
import webant.ru.application.ui.view.CameraSurfaceFragment
import java.io.File


class CameraFragment : CameraSurfaceFragment() {

    override val layoutId = R.layout.fragment_camera
    private val mediaPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).path!!
     val mediaFile: File
        get() {
            return File(mediaPath, "${fileName.text}.mp4")
        }
    private val photoFile: File
        get() {
            return File(mediaPath, "${fileName.text}.jpg")
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val user = listener?.getUser()
        if (user != null) {
            image.loadImage(user.photoUrl.toString())
            name.text = user.displayName
            email.text = user.email
        }
        videoButton.setOnClickListener {
            if (!isRecording) {
                Toast.makeText(context,"start record",Toast.LENGTH_LONG).show()
                startRecord(mediaFile)
            } else {
                Toast.makeText(context,"end record",Toast.LENGTH_LONG).show()
                stopRecord()
            }
        }
        photoButton.setOnClickListener {
            makePicture(photoFile)
        }
    }

    override fun requestHolder(): SurfaceHolder? = cameraView.holder
}
