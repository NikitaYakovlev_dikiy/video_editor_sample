package webant.ru.application.ui.view

import android.content.Context
import android.os.Environment
import org.m4m.MediaComposer
import ru.webant.video.editor.BaseVideoEditor
import java.io.File

class JoinVideo(context: Context) : BaseVideoEditor(context) {
    override var videoDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).path!!

    override var firstVideo = "$videoDirectory/fileName.mp4"
    private var secondVideo = "$videoDirectory/fileName1.mp4"
    override var outVideo = "$videoDirectory/outVideo1.mp4"

    fun loadFileList() = File(videoDirectory).listFiles()
        .filter { it.name.split(".").last() == "mp4" }
        .map { it.name }

    fun createFile(
        firstFileName: String,
        secondFileName: String,
        outFileName: String,
        onProgress: (Float) -> Unit,
        onComplete: (String) -> Unit
    ) {
        firstVideo = "$videoDirectory/$firstFileName"
        secondVideo = "$videoDirectory/$secondFileName"
        outVideo = "$videoDirectory/$outFileName.mp4"
        this.onComplete = onComplete
        this.onProgress = onProgress
        startTranscode()
    }

    override fun setTranscodeParameters(mediaComposer: MediaComposer) {
        super.setTranscodeParameters(mediaComposer)
        mediaComposer.addSourceFile(secondVideo)

    }

    override fun onError(message: Throwable) {
        message.printStackTrace()
    }
}
