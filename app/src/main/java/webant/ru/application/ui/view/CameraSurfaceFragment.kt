package webant.ru.application.ui.view

import android.graphics.Matrix
import android.graphics.RectF
import android.hardware.Camera
import android.media.CamcorderProfile
import android.media.MediaRecorder
import android.os.Bundle
import android.view.Surface
import android.view.SurfaceHolder
import android.view.View
import kotlinx.android.synthetic.main.fragment_camera.*
import java.io.File
import java.io.FileOutputStream

abstract class CameraSurfaceFragment : NavigationFragment() {

    private var camera: Camera? = null
    private var holder: SurfaceHolder? = null
    private var mediaRecorder: MediaRecorder? = null
    var isRecording = false


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initCamera()
        super.onViewCreated(view, savedInstanceState)
    }

    fun makePicture(outFile: File) {
        camera?.takePicture(null, null, Camera.PictureCallback { data, camera ->
            Thread {
                try {
                    val fileStream = FileOutputStream(outFile)
                    fileStream.write(data)
                    fileStream.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }.start()
            camera.startPreview()
        })
    }

    fun startRecord(outFile: File) {
        if (prepareRecorder(outFile)) {
            mediaRecorder?.start()
            isRecording = true
        } else {
            releaseRecorder()
        }
    }

    fun stopRecord() {
        if (mediaRecorder != null) {
            mediaRecorder!!.stop()
            isRecording = false
            releaseRecorder()
        }
    }

    private fun initCamera() {
        holder = requestHolder()
        holder?.addCallback(surfaceListener)
        holder?.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)
    }

    override fun onResume() {
        camera = Camera.open(0)
        setPreviewSize()
        super.onResume()
    }
    override fun onPause() {
        super.onPause()
        releaseRecorder()
        camera?.release()
        camera = null
    }

    private fun prepareRecorder(outFile: File): Boolean {

        camera ?: return false
        camera!!.unlock()

        mediaRecorder = MediaRecorder()

        mediaRecorder!!.setCamera(camera)
        mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.CAMCORDER)
        mediaRecorder!!.setVideoSource(MediaRecorder.VideoSource.CAMERA)
        mediaRecorder!!.setProfile(
            CamcorderProfile
                .get(CamcorderProfile.QUALITY_HIGH)
        )
        mediaRecorder!!.setOutputFile(outFile.absolutePath)
        mediaRecorder!!.setPreviewDisplay(cameraView.holder.surface)

        try {
            mediaRecorder!!.prepare()
        } catch (e: Exception) {
            e.printStackTrace()
            releaseRecorder()
            return false
        }

        return true
    }

    private fun releaseRecorder() {
        mediaRecorder?.reset()
        mediaRecorder?.release()
        mediaRecorder = null
        camera?.lock()
    }

    private val surfaceListener = object : SurfaceHolder.Callback {
        override fun surfaceDestroyed(holder: SurfaceHolder?) {}
        override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
            camera?.stopPreview()
            camera?.setPreviewDisplay(holder)
            setCameraDisplayOrientation(0)
            camera?.startPreview()
        }

        override fun surfaceCreated(holder: SurfaceHolder?) {
            camera?.setPreviewDisplay(holder)
            camera?.startPreview()
        }
    }


    private fun setPreviewSize() {
        val display = activity?.windowManager?.defaultDisplay

        display ?: return
        camera ?: return

        val widthIsMax = display.width > display.height
        val size = camera!!.parameters.previewSize

        val rectDisplay = RectF()
        val rectPreview = RectF()

        rectDisplay.set(0f, 0f, display.width.toFloat(), display.height.toFloat())
        if (widthIsMax) {
            rectPreview.set(0f, 0f, size.width.toFloat(), size.height.toFloat())
        } else {
            rectPreview.set(0f, 0f, size.height.toFloat(), size.width.toFloat())
        }

        val matrix = Matrix()
        matrix.setRectToRect(
            rectPreview, rectDisplay,
            Matrix.ScaleToFit.START
        )
        matrix.mapRect(rectPreview)
    }

    private fun setCameraDisplayOrientation(cameraId: Int) {
        // определяем насколько повернут экран от нормального положения
        val rotation = activity!!.windowManager.defaultDisplay.rotation
        var degrees = 0
        when (rotation) {
            Surface.ROTATION_0 -> degrees = 0
            Surface.ROTATION_90 -> degrees = 90
            Surface.ROTATION_180 -> degrees = 180
            Surface.ROTATION_270 -> degrees = 270
        }

        var result = 0

        // получаем инфо по камере cameraId
        val info = Camera.CameraInfo()
        Camera.getCameraInfo(cameraId, info)

        // задняя камера
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
            result = 360 - degrees + info.orientation
        } else
        // передняя камера
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = 360 - degrees - info.orientation
                result += 360
            }
        result %= 360
        camera?.setDisplayOrientation(result)
    }
    abstract fun requestHolder(): SurfaceHolder?
}