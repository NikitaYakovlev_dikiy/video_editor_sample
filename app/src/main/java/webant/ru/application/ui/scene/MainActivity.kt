package webant.ru.application.ui.scene

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import webant.ru.application.R
import webant.ru.application.core.CONST

class MainActivity : AppCompatActivity(), OnFragmentInteractionListener {

    private lateinit var navigation: NavController
    var userAccount: GoogleSignInAccount? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigation = Navigation.findNavController(this, R.id.navFragment)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                arrayOf(
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.RECORD_AUDIO,
                    android.Manifest.permission.CAMERA
                ), 0
            )
        }

    }

    override fun onStartAction(res: Int) {
        val bundle = Bundle()
        navigation.navigate(res, bundle)
    }

    override fun signIn() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(CONST.FB_CLIENT_ID)
            .requestEmail()
            .build()
        val signInIntent = GoogleSignIn.getClient(this, gso).signInIntent
        startActivityForResult(signInIntent, REQUEST_GOOGLE_SIGN)
    }

    private fun signRequest(data: Intent?) {
        val task = GoogleSignIn.getSignedInAccountFromIntent(data)
        try {
            val account = task.getResult(ApiException::class.java)
            userAccount = account
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun getUser(): GoogleSignInAccount? {
        return userAccount
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_GOOGLE_SIGN) {
            signRequest(data)
        }
    }

    companion object {
        const val REQUEST_GOOGLE_SIGN = 8234
    }
}
