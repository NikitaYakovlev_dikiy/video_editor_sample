package webant.ru.application.ui.scene

import android.net.Uri
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.FirebaseUser

interface OnFragmentInteractionListener {
    fun onStartAction(res: Int)
    fun signIn()
    fun getUser():GoogleSignInAccount?
}