package webant.ru.application.ui.scene.login

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_login.*
import webant.ru.application.R
import webant.ru.application.ui.view.NavigationFragment


class LoginFragment : NavigationFragment() {
    override val layoutId = R.layout.fragment_login

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        googleLoginB.setOnClickListener {
            listener?.signIn()
        }
        cameraButton.setOnClickListener {
            listener?.onStartAction(R.id.startCamera)
        }
        videoButton.setOnClickListener {
            listener?.onStartAction(R.id.startVideo)
        }
    }
}
