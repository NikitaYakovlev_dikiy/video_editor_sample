package webant.ru.application.ui.scene.video

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.fragment_video.*
import webant.ru.application.R
import webant.ru.application.ui.view.JoinVideo
import webant.ru.application.ui.view.NavigationFragment
import webant.ru.application.ui.view.TextVideo


class VideoFragment : NavigationFragment() {
    private val joinVideo by lazy { JoinVideo(context!!) }
    private val videoText by lazy { TextVideo(context!!) }
    override val layoutId = R.layout.fragment_video
    var videoList: List<String>? = null
    var firstSelected = ""
    var secondSelected = ""
    var effect = arrayListOf("Склеить", "добавить текст")
    var effectMode = 0


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateSpinner()
        outButton.setOnClickListener {
            if (outFile.text.toString().isEmpty())
                return@setOnClickListener
            if (effectMode == 0) {
                initJoinVideo()
            } else {
                initTextVideo()
            }
        }
        buttonLock.setOnClickListener {
            firstVideo.drawerModel ?: return@setOnClickListener
            firstVideo.isEnableMove = !firstVideo.isEnableMove
        }
        textInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                firstVideo.pushText(s.toString())
            }
        })
        openButton.setOnClickListener {
            openSetting()
        }
        closeButton.setOnClickListener {
            closeSetting()
        }
        animationButton.setOnClickListener {
            firstVideo.isAnimation=!firstVideo.isAnimation
        }
    }

    fun closeSetting() {
        settingLayout.visibility = View.GONE
        openButton.show()
    }

    fun openSetting() {
        settingLayout.visibility = View.VISIBLE
        openButton.hide()
    }

    fun initTextVideo() {
        firstVideo.drawerModel ?: return
        firstVideo.drawerModel!!.text = textInput.text.toString()
        videoText.createFile(
            firstSelected,
            outFile.text.toString(),
            firstVideo.drawerModel!!
            , {
                activity?.runOnUiThread {
                    progressBar.progress = (it * 100).toInt()
                }
            }, {
                activity?.runOnUiThread {
                    Toast.makeText(context, "complete", Toast.LENGTH_LONG).show()
                    updateSpinner()
                    loadVideo(it)
                }
            })
    }

    fun initJoinVideo() {
        joinVideo.createFile(
            firstSelected,
            secondSelected,
            outFile.text.toString()
            , {
                activity?.runOnUiThread {
                    progressBar.progress = (it * 100).toInt()
                }
            }, {
                activity?.runOnUiThread {
                    Toast.makeText(context, "complete", Toast.LENGTH_LONG).show()
                    updateSpinner()
                    loadVideo(it)
                }
            })
    }

    private fun updateSpinner() {
        videoList = joinVideo.loadFileList()
        effectSpinner.initSpinner(effect) {
            effectMode = effect.indexOf(it)
            if (effectMode == 0) {
                textInput.visibility = View.GONE
                secondSpinner.visibility = View.VISIBLE
            } else {
                textInput.visibility = View.VISIBLE
                secondSpinner.visibility = View.GONE
            }
        }
        firstSpinner.initSpinner(videoList!!) {
            firstSelected = it
            loadVideo("${joinVideo.videoDirectory}/$it")
        }
        secondSpinner.initSpinner(videoList!!) {
            secondSelected = it
            loadVideo("${joinVideo.videoDirectory}/$it")
        }
    }

    private fun loadVideo(path: String) {
        firstVideo.videoView.apply {
            setVideoPath(path)
            setMediaController(MediaController(context))
            start()
            setOnClickListener { v ->
                closeSetting()
            }
        }
    }

    private fun Spinner.initSpinner(items: List<String>, onSelect: (String) -> Unit) {
        val adapter = ArrayAdapter(
            context,
            android.R.layout.simple_spinner_dropdown_item, items
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        this.adapter = adapter
        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                onSelect.invoke(items[position])
            }
        }
    }


}