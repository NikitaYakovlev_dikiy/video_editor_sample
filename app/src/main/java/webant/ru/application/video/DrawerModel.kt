package webant.ru.application.video

import android.graphics.Canvas
import ru.webant.video.view.BaseDrawerModel
import ru.webant.video.view.VideoDrawerModel

class DrawerModel(width: Int, height: Int) : BaseDrawerModel(width, height) {
    var animationStart = 0L
    var animationEnd = 3000L

    override fun onClick(xy: Pair<Int, Int>) {
        x = xy.first
        y = xy.second
    }

    override fun drawOnView(canvas: Canvas, x: Float, y: Float, toX: Float?, toY: Float?) {
        canvas.translate(x, y)
        sampleCreateText(text, canvas.width, 1F, 1F, canvas)
        canvas.translate(-x, -y)
        if (toX != null && toY != null) {
            canvas.drawLine(x, y, toX, toY, paint)
            canvas.translate(toX, toY)
            paintAlpha = 100
            sampleCreateText(text, canvas.width, 1F, 1F, canvas)
            paintAlpha = 255
            canvas.translate(-toX, -toY)
        }
    }

    override fun drawOnVideo(canvas: Canvas, playtime: Long, videoDrawerModel: VideoDrawerModel) {
        var x = videoDrawerModel.x
        var y = videoDrawerModel.y
        val endX = videoDrawerModel.toX
        val endY = videoDrawerModel.toY
        val rotate = videoDrawerModel.rotate
        val horizontalSide = videoDrawerModel.horizontalSide

        if (endX != null && endY != null) {
            if (playtime in animationStart..animationEnd) {
                val maxTime = animationEnd - animationStart
                val progress = playtime.toFloat() / maxTime.toFloat()
                val translateX = endX - x
                val translateY = endY - y

                x += translateX * progress
                y += translateY * progress
            } else if (playtime > animationEnd) {
                x = endX
                y = endY
            }
        }

        canvas.translate(x, y)
        canvas.rotate(-rotate)
        sampleCreateText(text, horizontalSide, heightScale!!, widthScale!!, canvas)
        canvas.rotate(rotate)
        canvas.translate(-x, -y)
    }

    fun setAnimationPosition(xy: Pair<Int, Int>) {
        endX = xy.first
        endY = xy.second
    }
}