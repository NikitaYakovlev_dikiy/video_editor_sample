package webant.ru.application.video

import android.graphics.Canvas
import org.m4m.domain.graphics.IEglUtil
import ru.webant.video.effect.BaseTextOverlay

class TextOverlay(angle: Int, eglUtil: IEglUtil, private val drawerModel: DrawerModel) : BaseTextOverlay(angle, eglUtil) {
    override fun drawCanvas(canvas: Canvas, currentVideoTime: Long) {
        drawerModel.drawOnVideo(canvas, currentVideoTime)
    }
}