package webant.ru.application.video

import android.content.Context
import android.media.MediaPlayer
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import ru.webant.video.view.BaseVideoDrawer

class VideoEditorView(
    context: Context, attrs: AttributeSet
) : BaseVideoDrawer<DrawerModel>(context, attrs) {

    override val drawView = VideoDrawer(context)
    var isEnableMove: Boolean = false
    var isAnimation: Boolean = false

    override fun initDrawerModel(player: MediaPlayer): DrawerModel {
        return DrawerModel(player.videoWidth, player.videoHeight)
    }

    override fun onTouch(view: View, event: MotionEvent): Boolean {
        if (!isAnimation) {
            drawerModel?.getValidPosition(view.width, view.height, event.x, event.y)
                ?.let(drawerModel!!::onClick)
        } else {
            drawerModel?.getValidPosition(view.width, view.height, event.x, event.y)
                ?.let(drawerModel!!::setAnimationPosition)
        }
        drawView.drawerModel = drawerModel
        return isEnableMove
    }

    fun pushText(text: String) {
        drawerModel?.text = text
        drawView.drawerModel = drawerModel
    }

}