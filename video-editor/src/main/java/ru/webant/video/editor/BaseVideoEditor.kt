package ru.webant.video.editor

import android.content.Context
import android.util.Log
import org.m4m.AudioFormat
import org.m4m.Uri
import org.m4m.VideoFormat
import org.m4m.android.AndroidMediaObjectFactory
import org.m4m.android.AudioFormatAndroid
import org.m4m.android.VideoFormatAndroid

abstract class BaseVideoEditor(var context: Context) {
    abstract var videoDirectory: String
    abstract var firstVideo: String
    abstract var outVideo: String
    private var audioFormat: org.m4m.AudioFormat? = null
    private var videoFormat: org.m4m.VideoFormat? = null
    var mediaFileInfo: org.m4m.MediaFileInfo? = null
    private var duration: Long = 0
    private var videoWidthIn = 640
    private var videoHeightIn = 480
    var factory: AndroidMediaObjectFactory? = null
    private val videoMimeType = "video/avc"
    private var videoBitRateInKBytes = 5000
    private var videoFrameRate = 30
    private var videoIFrameInterval = 1
    private val audioMimeType = "audio/mp4a-latm"
    private var audioBitRate = 96 * 1024
    private var audioProfile = 0
    private var mediaComposer: org.m4m.MediaComposer? = null

    var onComplete: ((String) -> Unit)? = null
    var onProgress: ((Float) -> Unit)? = null


    open fun setTranscodeParameters(mediaComposer: org.m4m.MediaComposer) {
        mediaComposer.addSourceFile(firstVideo)
        mediaComposer.setTargetFile(outVideo, mediaFileInfo!!.rotation)
        configureVideoEncoder(mediaComposer, videoWidthIn, videoHeightIn)
        configureAudioEncoder(mediaComposer)
    }

    private fun configureVideoEncoder(mediaComposer: org.m4m.MediaComposer, width: Int, height: Int) {
        val videoFormat = VideoFormatAndroid(videoMimeType, width, height)
        videoFormat.videoBitRateInKBytes = videoBitRateInKBytes
        videoFormat.videoFrameRate = videoFrameRate
        videoFormat.videoIFrameInterval = videoIFrameInterval
        mediaComposer.targetVideoFormat = videoFormat
    }

    private fun configureAudioEncoder(mediaComposer: org.m4m.MediaComposer) {
        audioFormat ?: return
        val aFormat =
            AudioFormatAndroid(
                audioMimeType,
                audioFormat!!.audioSampleRateInHz,
                audioFormat!!.audioChannelCount
            )
        aFormat.audioBitrateInBytes = audioBitRate
        aFormat.audioProfile = audioProfile
        mediaComposer.targetAudioFormat = aFormat
    }

    private fun transcode() {
        factory = AndroidMediaObjectFactory(context)
        mediaComposer = org.m4m.MediaComposer(factory, progressListener)
        setTranscodeParameters(mediaComposer!!)
        mediaComposer!!.start()
    }

    open fun startTranscode() {
        getFileInfo()
        try {
            transcode()
        } catch (e: Exception) {
            onError(e)
        }
    }

    fun stopTranscode() {
        mediaComposer?.stop()
    }

    private fun getFileInfo() {
        try {
            mediaFileInfo = org.m4m.MediaFileInfo(AndroidMediaObjectFactory(context))
            mediaFileInfo!!.setPath(firstVideo)
            duration = mediaFileInfo!!.durationInMicroSec
            audioFormat = mediaFileInfo!!.audioFormat as? AudioFormat
            if (audioFormat == null) {
                onError(Throwable("Audio format info unavailable"))
            } else {
                try {
                    audioBitRate = audioFormat!!.audioBitrateInBytes
                    audioProfile = audioFormat!!.audioProfile
                } catch (e: Exception) {
                    audioFormat = null
                }
            }
            videoFormat = mediaFileInfo!!.videoFormat as VideoFormat?
            if (videoFormat == null) {
                onError(Throwable("Video format info unavailable"))
            } else {
                videoFrameRate = videoFormat!!.videoFrameRate
                videoWidthIn = videoFormat!!.videoFrameSize.width()
                videoHeightIn = videoFormat!!.videoFrameSize.height()
            }
        } catch (e: Exception) {
            onError(e)
        }
    }

    private fun org.m4m.MediaFileInfo.setPath(path: String) {
        this.uri = Uri(path)
    }

    abstract fun onError(message: Throwable)

    var progressListener: org.m4m.IProgressListener = object : org.m4m.IProgressListener {
        override fun onMediaStart() {}
        override fun onMediaPause() {}
        override fun onMediaStop() {}
        override fun onMediaProgress(progress: Float) {
            onProgress?.invoke(progress)
            Log.v("testLogProgress", "progress$progress")
        }

        override fun onMediaDone() {
            onComplete?.invoke(outVideo)
            Log.v("testLogProgress", "complete")
        }

        override fun onError(exception: Exception) {
            exception.printStackTrace()
            Log.v("testLogProgress", "error")
        }
    }
}