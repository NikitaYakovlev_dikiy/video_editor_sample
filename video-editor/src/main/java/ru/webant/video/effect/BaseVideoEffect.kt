package ru.webant.video.effect

import org.m4m.android.graphics.VideoEffect
import org.m4m.domain.graphics.IEglUtil

abstract class BaseVideoEffect(angle: Int, eglUtil: IEglUtil) : VideoEffect(angle, eglUtil) {
    var currentVideoTime = 0L
    override fun applyEffect(inputTextureId: Int, timeProgress: Long, transformMatrix: FloatArray?) {
        //is nano sec
        currentVideoTime = (timeProgress / 1000)
        super.applyEffect(inputTextureId, timeProgress, transformMatrix)
    }
}