/*
 * Copyright 2014-2016 Media for Mobile
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.webant.video.effect

import android.graphics.Canvas
import org.m4m.domain.graphics.IEglUtil
import ru.webant.video.view.BaseDrawerModel

abstract class BaseTextOverlay(
    angle: Int,
    eglUtil: IEglUtil
) : BaseOverlayEffect(angle, eglUtil) {

    override fun drawCanvas(canvas: Canvas) {
        drawCanvas(canvas, currentVideoTime)
    }

    abstract fun drawCanvas(canvas: Canvas, currentVideoTime: Long)
}
