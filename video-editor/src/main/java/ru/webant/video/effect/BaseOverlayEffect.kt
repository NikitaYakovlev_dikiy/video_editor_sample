package ru.webant.video.effect

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.opengl.GLES20
import android.opengl.GLUtils
import org.m4m.domain.graphics.IEglUtil

abstract class BaseOverlayEffect(angle: Int, eglUtil: IEglUtil) : BaseVideoEffect(angle, eglUtil) {

    private var oTextureHandle: Int = 0
    private val textures = IntArray(1)

    private var bitmap: Bitmap? = null
    private var inputBitmapWidth = 1280 // default resolution
    private var inputBitmapHeight = 720

    private val fragmentShader: String
        get() = "#extension GL_OES_EGL_image_external : require\n" +
                "precision mediump float;\n" +
                "varying vec2 vTextureCoord;\n" +
                "uniform samplerExternalOES sTexture;\n" +
                "uniform sampler2D oTexture;\n" +
                "void main() {\n" +
                "  vec4 bg_color = texture2D(sTexture, vTextureCoord);\n" +
                "  vec4 fg_color = texture2D(oTexture, vTextureCoord);\n" +
                "  float colorR = (1.0 - fg_color.a) * bg_color.r + fg_color.a * fg_color.r;\n" +
                "  float colorG = (1.0 - fg_color.a) * bg_color.g + fg_color.a * fg_color.g;\n" +
                "  float colorB = (1.0 - fg_color.a) * bg_color.b + fg_color.a * fg_color.b;\n" +
                "  gl_FragColor = vec4(colorR, colorG, colorB, bg_color.a);\n" +
                "}\n"


    init {
        bitmap = Bitmap.createBitmap(inputBitmapWidth, inputBitmapHeight, Bitmap.Config.ARGB_8888)
        setFragmentShader(fragmentShader)
    }

    override fun start() {
        super.start()
        oTextureHandle = shaderProgram.getAttributeLocation("oTexture")

        GLES20.glGenTextures(1, textures, 0)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0])
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR)
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR)
    }

    override fun addEffectSpecific() {
        if (bitmap!!.width != inputResolution.width()) {
            bitmap = Bitmap.createBitmap(inputResolution.width(), inputResolution.height(), Bitmap.Config.ARGB_8888)
        } else if (bitmap!!.height != inputResolution.height()) {
            bitmap = Bitmap.createBitmap(inputResolution.width(), inputResolution.height(), Bitmap.Config.ARGB_8888)
        }

        bitmap!!.eraseColor(Color.argb(0, 0, 0, 0))

        val bitmapCanvas = Canvas(bitmap!!)

        drawCanvas(bitmapCanvas)

        GLES20.glActiveTexture(GLES20.GL_TEXTURE1)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0])
        checkGlError("glBindTexture")


        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, bitmap, 0)
        checkGlError("texImage2d")
        GLES20.glUniform1i(oTextureHandle, 1)
        checkGlError("oTextureHandle - glUniform1i")
    }

    protected fun finalize() {}

    protected abstract fun drawCanvas(canvas: Canvas)
}
