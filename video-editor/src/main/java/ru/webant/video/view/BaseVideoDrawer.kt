package ru.webant.video.view

import android.annotation.SuppressLint
import android.content.Context
import android.media.MediaPlayer
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout
import android.widget.VideoView

@SuppressLint("ClickableViewAccessibility")
abstract class BaseVideoDrawer<T : BaseDrawerModel>(context: Context, attrs: AttributeSet) :
    FrameLayout(context, attrs) {

    var drawerModel: T? = null
        set(value) {
            field = value
            drawView.drawerModel = field
        }
    var videoView: VideoView = VideoView(context).apply {
        layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
    }
    abstract val drawView: BaseDrawerView<T>

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        this.addView(videoView)
        this.addView(drawView)
        videoView.setOnInfoListener(object : MediaPlayer.OnInfoListener {
            override fun onInfo(mp: MediaPlayer, what: Int, extra: Int): Boolean {
                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    drawView.layoutParams = LayoutParams(videoView.width, videoView.height)
                    drawerModel = initDrawerModel(mp)
                }
                return false
            }
        })
        videoView.setOnTouchListener { v, event ->
            drawerModel ?: return@setOnTouchListener false
            return@setOnTouchListener onTouch(videoView, event)

        }
    }

    abstract fun initDrawerModel(player: MediaPlayer): T

    abstract fun onTouch(view: View, event: MotionEvent): Boolean

}