package ru.webant.video.view

import android.graphics.*
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint

abstract class BaseDrawerModel(
    var videoWidth: Int,
    var videoHeight: Int
) {
    var x: Int? = null
    var y: Int? = null
    var endX: Int? = null
    var endY: Int? = null
    var text: String = ""
    var heightScale: Float? = null
    var widthScale: Float? = null
    var videoDrawerModel: VideoDrawerModel? = null
    val paint = Paint().apply {
        color = Color.RED
        style = Paint.Style.FILL
    }
    var paintAlpha = 255

    fun getValidPosition(width: Int, height: Int, touchX: Float, touchY: Float): Pair<Int, Int> {
        heightScale = height.toFloat() / videoHeight
        widthScale = width.toFloat() / videoWidth
        return Pair((touchX / widthScale!!).toInt(), (touchY / heightScale!!).toInt())
    }

    abstract fun onClick(xy: Pair<Int, Int>)

    fun drawOnView(canvas: Canvas) {
        if (x != null && y != null && widthScale != null && heightScale != null) {
            val startY = (y!! * heightScale!!)
            val startX = (x!! * widthScale!!)

            var toY: Float? = null
            var toX: Float? = null
            if (endY != null && endX != null) {
                toY = (endY!! * heightScale!!)
                toX = (endX!! * widthScale!!)
            }
            drawOnView(canvas, startX, startY, toX, toY)
        }
    }

    abstract fun drawOnView(canvas: Canvas, x: Float, y: Float, toX: Float?, toY: Float?)

    fun drawOnVideo(canvas: Canvas, playtime: Long) {
        if (playtime == 0L) {
            videoDrawerModel = null
        }
        if (x != null && y != null && widthScale != null && heightScale != null) {
            if (videoDrawerModel == null) {
                videoDrawerModel = VideoDrawerModel(canvas, videoWidth, videoHeight, x!!, y!!, endX, endY)
            }

//            var x = videoDrawerModel!!.x
//            var y = videoDrawerModel!!.y
//            val rotate = videoDrawerModel!!.rotate
//            val horizontalSide = videoDrawerModel!!.horizontalSide

//            val translate = if (rotate != 0F) {
//                horizontalSide - y
//            } else {
//                x
//            }
//
//            var translateProgress: Float? = null
//            if (playtime < animationTime)
//                translateProgress = playtime / animationTime.toFloat()
//            val animationTranslate = if (translateProgress != null) {
//                (horizontalSide - translate) * (1 - translateProgress)
//            } else {
//                0F
//            }
//
//            if (rotate != 0F) {
//                y += animationTranslate
//            } else {
//                x += animationTranslate
//            }
//            canvas.translate(x, y)
//            canvas.rotate(-rotate)
            drawOnVideo(canvas, playtime, videoDrawerModel!!)
//            sampleCreateText(text, horizontalSide, heightScale!!, widthScale!!, canvas)
//            canvas.rotate(rotate)
//            canvas.translate(-x, -y)
        }
    }

    abstract fun drawOnVideo(canvas: Canvas, playtime: Long, videoDrawerModel: VideoDrawerModel)

    fun sampleCreateText(
        text: String,
        maxWidth: Int,
        heightScale: Float,
        widthScale: Float,
        canvas: Canvas
    ) {

        val correctWidth = (heightScale + widthScale) / 2

        val textPadding = 4 / correctWidth
        val cornerRadius = 6F / correctWidth
        val textPaint = TextPaint()
        textPaint.color = Color.BLACK
        textPaint.alpha = paintAlpha
        textPaint.textAlign = Paint.Align.CENTER
        textPaint.textSize = 20F / correctWidth

        val drawingTextLayout =
            StaticLayout(text, textPaint, maxWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false)

        val rect = Rect()
        textPaint.getTextBounds(text, 0, text.length, rect)

        val textHeight = drawingTextLayout.height + textPadding * 2
        val textWidth = rect.width() + textPadding * 2

        val textBorder = RectF(
            0F,
            0F,
            textWidth,
            textHeight
        )
        paint.style = Paint.Style.FILL
        paint.color = Color.WHITE
        paint.alpha=paintAlpha
        canvas.drawRoundRect(textBorder, cornerRadius, cornerRadius, paint)

        paint.style = Paint.Style.STROKE
        paint.color = Color.BLACK
        paint.alpha=paintAlpha
        canvas.drawRoundRect(textBorder, cornerRadius, cornerRadius, paint)

        val translateX = textWidth / 2F
        val translateY = textPadding
        canvas.translate(translateX, translateY)
        drawingTextLayout.draw(canvas)
        canvas.translate(-translateX, -translateY)

    }
}