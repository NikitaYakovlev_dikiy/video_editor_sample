package ru.webant.video.view

import android.graphics.Canvas

class VideoDrawerModel(
    videoCanvas: Canvas,
    videoWidth: Int,
    videoHeight: Int,
    startX: Int,
    startY: Int,
    endX: Int?,
    endY: Int?
) {
    var rotate = 0F
    var horizontalSide = videoCanvas.width
    var x = 0F
    var y = 0F
    var toX: Float? = null
    var toY: Float? = null

    init {
        if (videoCanvas.height != videoHeight && videoCanvas.width != videoWidth &&
            videoCanvas.height == videoWidth && videoCanvas.width == videoHeight
        ) {
            rotate = 90F
            horizontalSide = videoCanvas.height
            y = horizontalSide - startX.toFloat()
            x = startY.toFloat()
            if (endX != null && endY != null) {
                toY = horizontalSide - endX.toFloat()
                toX = endY.toFloat()
            }
        } else {
            y = startY.toFloat()
            x = startX.toFloat()
            if (endX != null && endY != null) {
                toY = endY.toFloat()
                toX = endX.toFloat()
            }
        }

    }
}
