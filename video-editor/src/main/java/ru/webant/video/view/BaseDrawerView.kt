package ru.webant.video.view

import android.content.Context
import android.graphics.Canvas
import android.view.View


abstract class BaseDrawerView<T : BaseDrawerModel>(context: Context) : View(context) {
    var drawerModel: T? = null
        set(value) {
            field = value
            invalidate()
        }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawerModel?.drawOnView(canvas)
    }
}